// ==UserScript==
// @name         Google Image Search
// @namespace    Brakman
// @version      0.6
// @description  Adds an 'Image Search' option to the context menu (right-click) for images, which peform a search using the image on Google.
// @author       Brakman
// @include      http://*
// @include      https://*
// @matches      *://*/*
// @grant        unsafeWindow
// @run-at       document-body
// @noframes
// @require      https://code.jquery.com/jquery-3.2.0.js
// @downloadUrl  https://gitlab.com/Brakman/TM-GoogleImageSearch/raw/master/GoogleImageSearch.user.js
// ==/UserScript==

(function() {
    'use strict';
    if (unsafeWindow.top != unsafeWindow.self){
        return;
    }
    console.log("Initializing Image Search");
    jQuery.noConflict();
    var menu = jQuery("<menu type='context' id='imgSearchContextMenu'><menuitem id='imgSearchContextMenuItem' label='Search Image' ></menuitem></menu>");
    jQuery("html").append(menu);


    var imgObserver = new MutationObserver(function(mutations){

        mutations.forEach(function(mutation){
        	jQuery(mutation.target).find("img").each(function(){addContextMenu(this);});
            if(jQuery(mutation.target).is("img")){
            	addContextMenu(mutation.target);
            }
        });

    });

    function addContextMenu(image){
    	var url = jQuery(image).attr("src");
    	if(typeof url == 'undefined' || url.indexOf("data") === 0)
    		return;
        jQuery(image).attr("contextmenu", "imgSearchContextMenu");
    	jQuery(image).contextmenu(function(){
	        	unsafeWindow.imgSearchUrl  = jQuery(image).attr("src");
	        });
    }

    function imageSearch(){
        if(unsafeWindow.imgSearchUrl !== undefined && unsafeWindow.imgSearchUrl !== null && unsafeWindow.imgSearchUrl.length > 0){
        	var start = "";
        	if(imgSearchUrl.indexOf("//") === 0)
        		start = "http:";
        	else if(imgSearchUrl.indexOf("http") < 0)
        		start = "http://";
        	var url = "http://images.google.com/searchbyimage?site=search&image_url=" + start + unsafeWindow.imgSearchUrl;
            unsafeWindow.open(url);
        }
    }

    imgObserver.observe(document, {
    	childList: true, subtree: true
	});
	jQuery("#imgSearchContextMenuItem").click(function(){ imageSearch(); });
    jQuery("img").each(function(){addContextMenu(this);});
    console.log("Image Search Initialized");
})();